/* Zadanie nr 2 - obsługa błędów - interfejs
 *
 * Aleksander Matusiak (am347171)
 *
 * źródła: http://www.mimuw.edu.pl/~janowska/SO-LAB/03_pipe/,
 * http://www.mimuw.edu.pl/~janowska/SO-LAB/0607_pthreads/
 */

#ifndef _ERR_
#define _ERR_

/* print system call error message and terminate */
extern void syserr(int bl, const char *fmt, ...);

/* print system call error message and terminate 
 * used when only single thread is used and errno is valid
 */
extern void syserr_single(const char *fmt, ...);

/* print error message and terminate */
extern void fatal(const char *fmt, ...);

#endif
