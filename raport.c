/* Zadanie nr 2 - implementacja procesu raport
 * 
 * Aleksander Matusiak (am347171)
 * 
 * Przyjęte założenia niewynikające z treści zadania:
 * - frekwencja wypisywana jest z precyzją dwóch miejsc po przecinku,
 * - jeśli żadna komisja nie została jeszcze przetworzona, wypisywana
 * frekwencja wynosi 100.00%.
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include "mesg.h"
#include "err.h"

int main(int argc, char *argv[]) {
	// dla której listy mamy wypisać raport, -1 jeśli całościowy
    short l = -1; 
    if (argc >= 2)
        l = atoi(argv[1]) - 1; 

    // pobieranie identyfikatorów kolejek
    int init_qid, rep_qid;
    if ((init_qid = msgget(INITIATION_KEY, 0)) == -1)
        syserr_single("msgget");
    if ((rep_qid = msgget(REPORT_KEY, 0)) == -1)
        syserr_single("msgget");

    // wysyłanie wiadomości inicjującej
    Init init_mesg;
    init_mesg.mesg_type = REPORT_TYPE;
    init_mesg.process = getpid();
    init_mesg.val = l;
    if (msgsnd(init_qid, (char *) &init_mesg, INIT_DATA, 0) != 0)
		syserr_single("msgsnd");

    // odebranie i wypisanie ogólnych statystyk
    Report rep_mesg;
    if (msgrcv(rep_qid, &rep_mesg, REP_DATA, getpid(), 0) < 0)
        syserr_single("msgrcv");
    
    printf("Przetworzonych komisji: %d / %d\n", rep_mesg.commisions, rep_mesg.M);
    printf("Uprawnionych do głosowania: %ld\n", rep_mesg.entitled);
    printf("Głosów ważnych: %ld\n", rep_mesg.valid);
    printf("Głosów nieważnych: %ld\n", rep_mesg.invalid);
    double attendance;
    if (rep_mesg.entitled != 0) 
        attendance = (double)(rep_mesg.valid + rep_mesg.invalid) / rep_mesg.entitled * 100;
    else
        attendance = 100;
    printf("Frekwencja: %.2f%%\n", attendance);

    // odebranie i wypisanie danych dla kolejnych list
    Report_field rep_field_mesg;
    unsigned long results[rep_mesg.K];
    printf("Wyniki poszczególnych list:\n");
    int i, j;
    for (i = 0; i < rep_mesg.L; ++i) {
		long sum = 0;
		for (j = 0; j < rep_mesg.K; ++j) {
			if (msgrcv(rep_qid, &rep_field_mesg, REP_FIELD_DATA, getpid(), 0) < 0)
				syserr_single("msgrcv");
			results[j] = rep_field_mesg.result;
			sum += results[j];
		}
        if (l == -1)
            printf("%d ", i + 1);
        else
            printf("%d ", l + 1);

        printf("%ld ", sum);
        
        for (j = 0; j < rep_mesg.K; ++j)
            printf("%ld ", results[j]);
        printf("\n");
        
        if (l != -1)
            break;
    }
    
    return 0;
}
