/* Zadanie nr 2 - obsługa błędów - implementacja
 *
 * Aleksander Matusiak (am347171)
 *
 * źródła: http://www.mimuw.edu.pl/~janowska/SO-LAB/03_pipe/,
 * http://www.mimuw.edu.pl/~janowska/SO-LAB/0607_pthreads/
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include "err.h"

extern int sys_nerr;

void syserr(int bl, const char *fmt, ...)  {
    va_list fmt_args;

    fprintf(stderr, "ERROR: ");

    va_start(fmt_args, fmt);
    vfprintf(stderr, fmt, fmt_args);
    va_end (fmt_args);
    fprintf(stderr," (%d; %s)\n", bl, strerror(bl));
    exit(1);
}

void syserr_single(const char *fmt, ...)  {
    syserr(errno, fmt);
}

void fatal(const char *fmt, ...) {
    va_list fmt_args;

    fprintf(stderr, "ERROR: ");

    va_start(fmt_args, fmt);
    vfprintf(stderr, fmt, fmt_args);
    va_end (fmt_args);

    fprintf(stderr,"\n");
    exit(1);
}
