/* Zadanie nr 2 - plik nagłówkowy z definicjami typów oraz
 * identyfikatorami kolejek
 * 
 * Aleksander Matusiak (am347171)
 */

#define COMMISION_TYPE 1
#define REPORT_TYPE 2

// wiadomość od klienta do serwera informująca o rozpoczęciu pracy
typedef struct {
    long mesg_type; // COMMISION_TYPE lub REPORT_TYPE 
    // w przypadku komisji - jej numer, w przypadku raportu - numer
    // żądanej listy lub -1, jeśli żądamy całego raportu
    int val; 
    // identyfikator procesu odpowiadającego danemu klientowi
    pid_t process; 
} Init;
#define INIT_DATA sizeof(Init)-sizeof(long)

// odpowiedź od serwera do komisji potwierdzająca lub odrzucająca
// zaczęcie pracy
typedef struct {
    long mesg_type;
    short success; // 1 - sukces, 0 - odrzucenie
} Reply;
#define REPLY_DATA sizeof(Reply)-sizeof(long)

// wiadomość od komisji z liczbą wyborców oraz oddanych głosów
typedef struct {
    long mesg_type; // identyfikacja po identyfikatorze procesu
    int i, j;
} Comm_start;
#define COMM_START_DATA sizeof(Comm_start)-sizeof(long)

// wiadomość od komisji z danymi dotyczącymi głosowania na pojedynczego
// kandydata albo o końcu wysyłanych danych
typedef struct {
    long mesg_type; // identyfikacja po identyfikatorze procesu
    unsigned int l, k, n;
    // 1 - gdy koniec danych (wtedy l, k, n - nieustawione),
    // 0 - w przeciwnym przypadku
    short end;
    
} Entry;
#define ENTRY_DATA sizeof(Entry)-sizeof(long)

// wiadomość od serwera do komisji z potwierdzającymi statystykami
typedef struct {
    long mesg_type; //typ równy identyfikatorowi procesu komisji
    unsigned int w, sum_n;
} Completed;
#define COMP_DATA sizeof(Completed)-sizeof(long)

// wiadomość od serwera do raportu z ogólnymi statystykami
typedef struct {
    long mesg_type; //typ równy identyfikatorowi procesu raportu
    unsigned short K, L;
    unsigned int M;
    unsigned int commisions;
    unsigned long entitled;
    unsigned long valid;
    unsigned long invalid;
} Report;
#define REP_DATA sizeof(Report)-sizeof(long)

// wiadomość od serwera do raportu z wynikami pojedynczej listy
typedef struct {
    long mesg_type;
    // wyniki jednej listy na tablicy stałej długości, żeby istniała
    // możliwość odgórnego wyznaczenia rozmiaru przesyłanej struktury
    unsigned long result;
} Report_field;
#define REP_FIELD_DATA sizeof(Report_field)-sizeof(long)

// klucze kolejek

// kolejka odpowiadająca przesyłaniu danych inicjujących od klienta
// do serwera, odczytywana przez główny wątek
#define INITIATION_KEY 1322L

// kolejka odpowiadająca przesyłaniu danych od komisji do serwera, 
// odczytywana przez wątek przydzielony danej komisji
#define COMMISION_KEY 1235L

// kolejka odpowiadająca przesyłaniu danych kończących od serwera
// do komisji, odczytywana przez proces komisji
#define END_COMMISION_KEY 1535L

// kolejka odpowiadająca przesyłaniu danych od serwera do raportu
// z aktualnymi wynikami, odczytywana przez proces raportu
#define REPORT_KEY 1423L
