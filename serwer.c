/* Zadanie nr 2 - implementacja procesu serwer
 * 
 * Aleksander Matusiak (am347171)
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <pthread.h>
#include "mesg.h"
#include "err.h"

unsigned short L, K;
unsigned int M;
// identyfikatory kolejek odpowiadające kolejnym kluczom kolejek z mesg.h
int init_qid, comm_qid, end_comm_qid, rep_qid;

unsigned long **results; // wyniki globalne
pid_t *client_pid; // identyfikatory procesów komisji

// zmienne do liczenia i ochrony statystyk do raportu
unsigned int cnt_commisions; 
unsigned long cnt_entitled, cnt_valid, cnt_invalid;
pthread_mutex_t statistics;

// zmienne do obsługi maksymalnej liczby wątków
#define MAX_THREADS 100
unsigned short cnt_threads;
pthread_mutex_t mutex_cnt_threads;
pthread_cond_t FOR_THREADS; // kolejka do czekania na miejsce na wątki

// ochrona pojedynczych wartości w tablicy results
pthread_mutex_t **lock;

/* zmienne umożliwiające wzajemne wykluczanie dwóch grup:
 * wątków odpowiadających raportom oraz wątków odpowiadających komisjom.
 * Zastosowany mechanizm synchronizacji pozwala działać wątkom tylko
 * z jednej grupy, z czego dla raportów nie ma narzuconych dodatkowych
 * ograniczeń, natomiast dla komisji, w przypadku konieczności zmiany
 * danych, każda z nich jest chroniona przez odpowiedni mutex
 * (lock[i][j] lub statistics).
 * Grupa 0 - komisje, 1 - raporty.
 */ 
pthread_mutex_t mutex;
pthread_cond_t GROUP[2]; // kolejka na oczekujące procesy danej grupy
unsigned short in[2], out[2];

// czy wysłano sygnał zakończenia, 1 - jeśli tak, 0 - wpp
// używany w celu niezwracania błędu wynikającego z zamkniętej w obsłudze
// sygnału SIGINT kolejki 
short has_ended;

// inicjuje mutexy, zmienne warunkowe i blokady rwlock oraz 
// tworzy kolejki
void init_sync() {
    int err;
    int i, j;

    // dynamiczna alokacja zasobów
    results = malloc(L * sizeof(unsigned long *));
    lock = malloc(L * sizeof(pthread_mutex_t *));
    client_pid = malloc(M * sizeof(pid_t));
    if (results == NULL || lock == NULL || client_pid == NULL) 
        syserr_single("malloc");
    for (i = 0; i < L; ++i) {
        results[i] = malloc(K * sizeof(unsigned long));
        lock[i] = malloc(K * sizeof(pthread_mutex_t));
        if (results[i] == NULL || lock[i] == NULL)
            syserr_single("malloc");
    }
    for (i = 0; i < M; ++i)
        client_pid[i] = 0;
    
    // inicjowanie mechanizmów synchronizacji oraz macierzy wyników
    for (i = 0; i < L; ++i)
        for (j = 0; j < K; ++j) {
            results[i][j] = 0;
            if ((err = pthread_mutex_init(&lock[i][j], 0) != 0))
                syserr (err, "rwlock init failed");

        }
    if ((err = pthread_mutex_init(&mutex_cnt_threads, 0) != 0))
        syserr(err, "mutex init failed");
    if ((err = pthread_mutex_init(&statistics, 0) != 0))
        syserr(err, "mutex init failed");
    if ((err = pthread_mutex_init(&mutex, 0) != 0))
        syserr(err, "mutex init failed");
    for (i = 0; i < 2; ++i)
        if ((err = pthread_cond_init(&GROUP[i], 0)) != 0)
            syserr(err, "cond init failed");
    if ((err = pthread_cond_init(&FOR_THREADS, 0)) != 0)
        syserr(err, "cond init failed");

    // tworzenie kolejek odpowiadających odpowiednim kluczom
    if ((init_qid = msgget(INITIATION_KEY, 0666 | IPC_CREAT | IPC_EXCL)) == -1)
        syserr_single("msgget");
    if ((comm_qid = msgget(COMMISION_KEY, 0666 | IPC_CREAT | IPC_EXCL)) == -1)
        syserr_single("msgget");
    if ((end_comm_qid = msgget(END_COMMISION_KEY, 0666 | IPC_CREAT | IPC_EXCL)) == -1)
        syserr_single("msgget");
    if ((rep_qid = msgget(REPORT_KEY, 0666 | IPC_CREAT | IPC_EXCL)) == -1)
        syserr_single("msgget");
}

// sprawdzenie, czy nie przekroczyliśmy limitu wątków
// i ewentualne czekanie, a później aktualizacja liczby wątków
void wait_for_threads() {
    int err;
    
    if ((err = pthread_mutex_lock(&mutex_cnt_threads)) != 0)
        syserr(err, "lock failed");
    while (cnt_threads == MAX_THREADS)
        if ((err = pthread_cond_wait(&FOR_THREADS, &mutex_cnt_threads)) != 0)
            syserr (err, "cond wait failed");
    ++cnt_threads;
    if ((err = pthread_mutex_unlock(&mutex_cnt_threads)) != 0)
        syserr(err, "unlock failed");

}

// aktualizuje liczbę wątków i ewentualnie umożliwia utworzenie
// nowych wątków
void exit_thread() {
    int err;
    
    if ((err = pthread_mutex_lock(&mutex_cnt_threads)) != 0)
        syserr(err, "lock failed");
    --cnt_threads;
    if ((err = pthread_cond_signal(&FOR_THREADS)) != 0)
        syserr (err, "cond signal failed");
    if ((err = pthread_mutex_unlock(&mutex_cnt_threads)) != 0)
        syserr(err, "unlock failed");
}

// protokół wstępny, n - typ grupy (jw)
void entry_protocol(int n) {
    int err;
    
    if ((err = pthread_mutex_lock(&mutex)) != 0)
        syserr(err, "lock failed");
    // jeśli ktoś z przeciwnej grupy jest w środku albo czeka
    // (żeby nie było zagłodzenia)
    if (in[1 - n] != 0 || out[1 - n] != 0) {
        ++out[n];
        // możemy jedynie wielokrotnie sprawdzać warunek, czy kogoś
        // z grupy przeciwnej nie ma w środku. W przypadku ponownego
        // sprawdzania warunku jak wyżej może dojść do zakleszczenia,
        // gdy żaden proces nie znajduje się w sekcji krytycznej
        while (in[1-n] != 0)
            if ((err = pthread_cond_wait(&GROUP[n], &mutex)) != 0)
                syserr (err, "cond wait failed");
        --out[n];
    }
    ++in[n];
    // wpuszczamy pozostałych z naszej grupy
    if ((err = pthread_cond_signal(&GROUP[n])) != 0)
        syserr (err, "cond signal failed");
    if ((err = pthread_mutex_unlock(&mutex)) != 0)
        syserr(err, "unlock failed");
}

// protokół końcowy, n - typ grupy (jw)
void exit_protocol(int n) {
    int err;
    
    if ((err = pthread_mutex_lock(&mutex)) != 0)
        syserr(err, "lock failed");
    --in[n];
    if (in[n] == 0) // jeśli wychodzimy ostatni
        if ((err = pthread_cond_signal(&GROUP[1 - n])) != 0)
            syserr (err, "cond signal failed");
    if ((err = pthread_mutex_unlock(&mutex)) != 0)
        syserr(err, "unlock failed");
}

// wątek do obsługi komisji
void *service(void *data) {
    int m = *(int *)data;
    free(data);

    int err;

    // zmienne do liczenia wpisów i sumy głosów
    int w = 0, sum_n = 0;

    // tablica do lokalnego przechowywania wczytanych wyników
    int votes[L][K];
    unsigned short i, j;
    for (i = 0; i < L; ++i)
        for (j = 0; j < K; ++j)
            votes[i][j] = 0;

    // pobranie informacji o wyborcach
    Comm_start comm_start_mesg;
    if ((err = msgrcv(comm_qid, &comm_start_mesg, COMM_START_DATA, client_pid[m], 0)) <= 0) {
        if (has_ended) return 0;
        else syserr(err, "msgrcv");
    }

    // odczyt kolejnych danych
    Entry entry_mesg;
    Completed comp_mesg;
    for (;;) {
        if ((err = msgrcv(comm_qid, &entry_mesg, ENTRY_DATA, client_pid[m], 0)) <= 0) {
            if (has_ended) return 0;
            else syserr(err, "msgrcv");
		}
        if (entry_mesg.end) { // dostaliśmy komunikat o końcu danych
            // przesyłamy informacje o liczbie wpisów i sumie ważnych
            // głosów
            comp_mesg.mesg_type = client_pid[m];
            comp_mesg.w = w;
            comp_mesg.sum_n = sum_n;
            if ((err = msgsnd(end_comm_qid, (char *) &comp_mesg, COMP_DATA, 0)) != 0) {
                if (has_ended) return 0;
                else syserr(err, "msgsnd");
            }
            break;
        } else { // kolejne dane
            ++w;
            sum_n += entry_mesg.n;
            votes[entry_mesg.l][entry_mesg.k] += entry_mesg.n;
        }
    }
    
    entry_protocol(0); // wchodzimy do sekcji krytycznej
    // aktualizowanie globalnych wyników
    for (i = 0; i < L; ++i)
        for (j = 0; j < K; ++j) {
            // ochrona pojedynczych wartości
            if ((err = pthread_mutex_lock(&lock[i][j])) != 0)
                syserr(err, "lock failed");
            results[i][j] += votes[i][j];
            if ((err = pthread_mutex_unlock(&lock[i][j])) != 0)
                syserr(err, "unlock failed");
        }
    
    // aktualizacja statystyk
    if ((err = pthread_mutex_lock(&statistics)) != 0)
        syserr(err, "lock failed");
        
    ++cnt_commisions;
    cnt_entitled += comm_start_mesg.i;
    cnt_valid += sum_n;
    cnt_invalid += comm_start_mesg.j - sum_n;

    if ((err = pthread_mutex_unlock(&statistics)) != 0)
        syserr(err, "unlock failed");

    exit_protocol(0); // wychodzimy z sekcji krytycznej
    
    exit_thread();
    return 0;
}

// struktura do przekazywania argument do wątku obsługującego raport
typedef struct {
    pid_t pid;
    short l;
} report_data;

// wątek do obsługi raportu
void *report(void *data) {
    pid_t pid = (*(report_data *)data).pid;
    short l = (*(report_data *)data).l;
    free(data);
    int err;

    entry_protocol(1); // wchodzimy do sekcji krytycznej

    // odczytanie i wysłanie ogólnych statystyk
    Report rep_mesg;
    rep_mesg.mesg_type = pid;
    rep_mesg.K = K;
    rep_mesg.L = L;
    rep_mesg.M = M;
    rep_mesg.commisions = cnt_commisions;
    rep_mesg.entitled = cnt_entitled;
    rep_mesg.valid = cnt_valid;
    rep_mesg.invalid = cnt_invalid;
    
    if ((err = msgsnd(rep_qid, (char *) &rep_mesg, REP_DATA, 0)) != 0) {
        if (has_ended) return 0;
        else syserr(err, "msgsnd");
    }

    // wysyłanie wyników kolejnych (dla l != -1 - jednej) list wyborczych 
    Report_field rep_field_mesg;
    rep_field_mesg.mesg_type = pid;
    short i, j;
    if (l == -1)
        for (i = 0; i < L; ++i)
            for (j = 0; j < K; ++j) {
                rep_field_mesg.result = results[i][j];
                if ((err = msgsnd(rep_qid, (char *) &rep_field_mesg, REP_FIELD_DATA, 0)) != 0) {
                    if (has_ended) return 0;
                    else syserr(err, "msgsnd");
                }
            }
    else
        for (j = 0; j < K; ++j) {
                rep_field_mesg.result = results[l][j];
                if ((err = msgsnd(rep_qid, (char *) &rep_field_mesg, REP_FIELD_DATA, 0)) != 0) {
                    if (has_ended) return 0;
                    else syserr(err, "msgsnd");
                }
        }

    exit_protocol(1); // wychodzimy z sekcji krytycznej

    exit_thread();
    return 0;
}

// procedura zwalniająca kolejki - obsługa sygnału SIGINT
void exit_server(int sig) {
	has_ended = 1;
	
    if (msgctl(init_qid, IPC_RMID, 0) == -1)
        syserr_single("msgctl RMID");
    if (msgctl(comm_qid, IPC_RMID, 0) == -1)
        syserr_single("msgctl RMID");
    if (msgctl(end_comm_qid, IPC_RMID, 0) == -1)
        syserr_single("msgctl RMID");
    if (msgctl(rep_qid, IPC_RMID, 0) == -1)
        syserr_single("msgctl RMID");

	// nie zwalniam pamięci związanej z tablicami results, lock
	// i client_pid - i tak zostanie zwolniona, a zwolnienie jej
	// mogłoby skutkować próbą nadpisania jej przez inny wątek
    
    exit(0);
}

int main(int argc, char *argv[]) {
    
    if (argc < 4) return -1;
    
    L = atoi(argv[1]);
    K = atoi(argv[2]);
    M = atoi(argv[3]);

    // inicjacja mechanizmów synchronizacji i komunikacji oraz
    // alokacja globalnych zasobów
    init_sync();
    
    // obsługa sygnału wyjścia
    if (signal(SIGINT,  exit_server) == SIG_ERR)
        syserr_single("signal");

    // ogólny atrybut tworzonych wątków
    pthread_attr_t attr;
    if (pthread_attr_init(&attr) != 0)
        syserr_single("attr init failed");
    if (pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED) != 0)
        syserr_single("setdetachstate failed");

    // zmienna do zapisania identyfikatoru tworzonego wątku,
    // w związku z ustawieniem atrybuty DETACHED nie musimy tego
    // identyfikatora pamiętać
    pthread_t temp_thread;
    int err;
    Init init_mesg;
    for (;;) {
        //pobranie danych inicjujących
        if ((msgrcv(init_qid, &init_mesg, INIT_DATA, 0, 0)) <= 0) {
            if (has_ended) return 0;
            else syserr_single("msgrcv");
        }
        if (init_mesg.mesg_type == COMMISION_TYPE) {
            int *m = (int *) malloc (sizeof(int));
            if (m == NULL)
                syserr_single("malloc");
            *m = init_mesg.val;

            // wysłanie potwierdzenie lub odrzucenie rozpoczęcia pracy
            Reply reply_mesg;
            reply_mesg.mesg_type = init_mesg.process;
            reply_mesg.success = (client_pid[*m] == 0);
            if ((err = msgsnd(end_comm_qid, (char *) &reply_mesg, REPLY_DATA, 0)) != 0) {
                if (has_ended) return 0;
				else syserr(err, "msgsnd");
			}
            if (client_pid[*m]) {
				free(m);
                continue;
			}
            
            // tworzenie wątku
            client_pid[*m] = init_mesg.process;
            wait_for_threads();
            if ((err = pthread_create (&temp_thread, &attr, service, (void *)m)) != 0)
                syserr(err, "create failed");
        } else { // REPORT_TYPE
            report_data *data = (report_data *) malloc (sizeof(report_data));
            if (data == NULL)
                syserr_single("malloc");
            (*data).pid = init_mesg.process;
            (*data).l = init_mesg.val;
            
            // tworzenie wątku
            wait_for_threads();
            if ((err = pthread_create (&temp_thread, &attr, report, (void *)data)) != 0)
                syserr(err, "create failed");
        }
    }
    
    return 0;
}
