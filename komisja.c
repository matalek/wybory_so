/* Zadanie nr 2 - implementacja procesu komisja
 *
 * Aleksander Matusiak (am347171)
 *
 * Przyjęte założenia niewynikające z treści zadania:
 * - frekwencja wypisywana jest z precyzją dwóch miejsc po przecinku.
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include "mesg.h"
#include "err.h"

int main(int argc, char *argv[]) {

    if (argc < 2) return -1;
    int m = atoi(argv[1]) - 1;

    // pobieranie identyfikatorów kolejek
    int init_qid, comm_qid, end_comm_qid;
    if ((init_qid = msgget(INITIATION_KEY, 0)) == -1)
        syserr_single("msgget");
    if ((comm_qid = msgget(COMMISION_KEY, 0)) == -1)
        syserr_single("msgget");
    if ((end_comm_qid = msgget(END_COMMISION_KEY, 0)) == -1)
        syserr_single("msgget");

    // wysyłanie wiadomości inicjującej
    Init init_mesg;
    init_mesg.mesg_type = COMMISION_TYPE;
    init_mesg.val = m;
    init_mesg.process = getpid();
    if (msgsnd(init_qid, (char *) &init_mesg, INIT_DATA, 0) != 0)
        syserr_single("msgsnd");

    // odebranie informacji z informacją, czy możemy działać
    Reply reply_mesg;
    if (msgrcv(end_comm_qid, &reply_mesg, REPLY_DATA, getpid(), 0) < 0)
        syserr_single("msgrcv");
    if (!reply_mesg.success) {
        printf("Odmowa dostępu\n");
        return -1;
    }

    // wczytanie i przesłanie do serwera informacji o wyborcach
    int i, j;
    scanf("%d%d", &i, &j);

    Comm_start comm_start_mesg;
    comm_start_mesg.mesg_type = getpid();
    comm_start_mesg.i = i;
    comm_start_mesg.j = j;
    if (msgsnd(comm_qid, (char *) &comm_start_mesg, COMM_START_DATA, 0) != 0)
        syserr_single("msgsnd");

    // wczytywanie i przesyłanie wyników dla poszczególnych kandydatów
    Entry mesg;
    while (1) {
        if (scanf("%d%d%d", &mesg.l, &mesg.k, &mesg.n) != 3)
            break;
        --mesg.l; --mesg.k;
        mesg.end = 0;
        mesg.mesg_type = getpid();
        if (msgsnd(comm_qid, (char *) &mesg, ENTRY_DATA, 0) != 0)
            syserr_single("msgsnd");
    }

    // wysłanie komunikatu kończącego dane
    mesg.end = 1;
    mesg.mesg_type = getpid();
    if (msgsnd(comm_qid, (char *) &mesg, ENTRY_DATA, 0) != 0)
            syserr_single("msgsnd");

    // odebranie potwierdzenia
    Completed comp_mesg;
    if (msgrcv(end_comm_qid, &comp_mesg, COMP_DATA, getpid(), 0) < 0)
        syserr_single("msgrcv");

    // wypisanie statystyk
    int w = comp_mesg.w, sum_n = comp_mesg.sum_n;
    printf("Przetworzonych wpisów: %d\n", w);
    printf("Uprawnionych do głosowania: %d\n", i);
    printf("Głosów ważnych: %d\n", sum_n);
    printf("Głosów nieważnych: %d\n", j - sum_n);
    printf("Frekwencja w lokalu: %.2f%%\n", (double)j / i * 100);

    return 0;
}
